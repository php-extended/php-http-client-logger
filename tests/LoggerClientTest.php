<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-logger library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpClient\LoggerClient;
use PhpExtended\HttpMessage\Request;
use PhpExtended\HttpMessage\Response;
use PhpExtended\HttpMessage\Uri;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\AbstractLogger;
use Psr\Log\LoggerInterface;

/**
 * LoggerClientTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\HttpClient\LoggerClient
 *
 * @internal
 *
 * @small
 */
class LoggerClientTest extends TestCase
{
	
	/**
	 * The logger to help.
	 * 
	 * @var LoggerInterface
	 */
	protected LoggerInterface $_logger;
	
	/**
	 * The object to test.
	 * 
	 * @var LoggerClient
	 */
	protected $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testItWorks() : void
	{
		$request = new Request();
		$request = $request->withUri(new Uri());
		
		$this->_object->sendRequest($request);
		
		$this->assertCount(4, $this->_logger->logs);
	}
	
	public function testItExceptions() : void
	{
		$this->expectException(ClientExceptionInterface::class);
		
		$request = new Request();
		$request = $request->withUri(new Uri());
		$request = $request->withHeader('FAILED', '1');
		
		$this->_object->sendRequest($request);
		
		$this->assertCount(3, $this->_logger->logs);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$client = new class() implements ClientInterface
		{
			
			public function sendRequest(RequestInterface $request) : ResponseInterface
			{
				if($request->hasHeader('FAILED'))
				{
					throw new class() extends RuntimeException implements ClientExceptionInterface {};
				}
				
				return new Response();
			}
		};
		
		$this->_logger = new class() extends AbstractLogger
		{
			
			public $logs = [];
			
			public function log($level, $message, array $context = []) : void
			{
				$this->logs[] = ['level' => $level, 'message' => $message, 'context' => $context];
			}
		};
		
		$this->_object = new LoggerClient($client, $this->_logger);
	}
	
}
