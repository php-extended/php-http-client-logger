# php-extended/php-http-client-logger
A psr-18 compliant middleware client that logs psr-7 http messages.

![coverage](https://gitlab.com/php-extended/php-http-client-logger/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-http-client-logger/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-http-client-logger ^8`


## Basic Usage

This library is to make a man in the middle for http requests and responses
and logs the events when requests passes. It may be used the following way :

```php

/** @var Psr\Log\LoggerInterface $logger */            // psr-3
/** @var Psr\Http\Client\ClientInterface $client */    // psr-18
/** @var Psr\Http\Message\RequestInterface $request */ // psr-7

$client = new LoggerClient($logger, $client);
$response = $client->sendRequest($request);

/** @var Psr\Http\Message\ResponseInterface $response */

```


## License

MIT (See [license file](LICENSE)).
