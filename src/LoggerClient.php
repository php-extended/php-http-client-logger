<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-logger library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use DateTimeImmutable;
use DateTimeInterface;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use Stringable;

/**
 * LoggerClient class file.
 * 
 * This class is an implementation of a client which does logging each
 * time a request is passed by on a synchronous way.
 * 
 * @author Anastaszor
 */
class LoggerClient implements ClientInterface, Stringable
{
	
	/**
	 * The logger where to log all the messages.
	 * 
	 * @var LoggerInterface
	 */
	protected LoggerInterface $_logger;
	
	/**
	 * The real client.
	 * 
	 * @var ClientInterface
	 */
	protected ClientInterface $_client;
	
	/**
	 * Builds a new LoggerProcessor with the given logger and real processor.
	 * 
	 * @param ClientInterface $client
	 * @param LoggerInterface $logger
	 */
	public function __construct(ClientInterface $client, LoggerInterface $logger)
	{
		$this->_client = $client;
		$this->_logger = $logger;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Client\ClientInterface::sendRequest()
	 */
	public function sendRequest(RequestInterface $request) : ResponseInterface
	{
		$uristr = $request->getUri()->__toString();
		
		$message = 'Sending {verb} {uri}';
		$context = ['uri' => $uristr, 'verb' => $request->getMethod()];
		$this->_logger->info($message, $context);
		$this->_logger->debug('{headers}', ['headers' => $request->getHeaders()]);
		
		$before = new DateTimeImmutable();
		
		try
		{
			$response = $this->_client->sendRequest($request);
		}
		catch(ClientExceptionInterface $e)
		{
			$after = new DateTimeImmutable();
			$message = 'Exception while receiving in {time} for {uri} : {message}';
			$context = [
				'uri' => $uristr,
				'message' => $e->getMessage(),
				'time' => $this->formatTimeDiff($before, $after),
			];
			$this->_logger->error($message, $context);
			
			throw $e;
		}
		
		$after = new DateTimeImmutable();
		$message = 'Receiving in {time} from {uri} : {code} ({message})';
		$context = [
			'uri' => $uristr,
			'code' => $response->getStatusCode(),
			'message' => $response->getReasonPhrase(),
			'time' => $this->formatTimeDiff($before, $after),
		];
		$this->_logger->info($message, $context);
		$this->_logger->debug('{headers}', ['headers' => $response->getHeaders()]);
		
		return $response;
	}
	
	/**
	 * Formats the time difference between two datetime.
	 * 
	 * @param DateTimeInterface $before
	 * @param DateTimeInterface $after
	 * @return string
	 */
	public function formatTimeDiff(DateTimeInterface $before, DateTimeInterface $after) : string
	{
		$diff = $after->diff($before);
		
		$format = '';
		if(0 < $diff->y)
		{
			$format .= '%y Years, ';
		}
		if(0 < $diff->m)
		{
			$format .= '%m Months, ';
		}
		if(0 < $diff->d)
		{
			$format .= '%d Days, ';
		}
		if(0 < $diff->h)
		{
			$format .= '%h Hours, ';
		}
		if(0 < $diff->i)
		{
			$format .= '%i Minutes, ';
		}
		$format .= '%s Seconds, %f us';
		
		return $diff->format($format);
	}
	
}
